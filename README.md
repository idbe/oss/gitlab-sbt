[![pipeline status](https://gitlab.com/idbe/oss/gitlab-sbt/badges/development/pipeline.svg)](https://gitlab.com/idbe/oss/gitlab-sbt/commits/development)
[![coverage report](https://gitlab.com/idbe/oss/gitlab-sbt/badges/development/coverage.svg)](https://gitlab.com/idbe/oss/gitlab-sbt/commits/development)

# GitLab SBT Publish Plugin

This is an SBT plugin to add the appropriate headers required to publish Maven artifacts to GitLab.

This is mostly based on the response from: https://github.com/sbt/sbt/issues/4382#issuecomment-469734888

> Thanks to [alterationx10](https://gitlab.com/alterationx10) for their project [GitLab-SBT](https://gitlab.com/SLUDG/gitlab-sbt) 


## Usage

### Project Configuration

In your projects `project/plugins.sbt` file, add following lines - 
```
resolvers += "IDBE LABS OSS" at "https://gitlab.com/api/v4/groups/6418420/-/packages/maven"
addSbtPlugin("com.idbelabs.sbt" % "gitlab" % "0.1") 
```
where, `0.1` is the version of the plugin, replace `0.1` with whatever the latest version is. 

The latest version can be found at : [IDBE LABS OSS](https://gitlab.com/groups/idbe/oss/-/packages)

In your `build.sbt` file, add the following (with your project's appropriate gitlab id)

```
enablePlugins(GitlabPlugin)
com.idbelabs.sbt.gitlab.GitlabPlugin.gitlabProjectId := "12345"
```
where, `12345` is the Gitlab project id. Replace it with the id of your gitlab project id. 

### Execution

To publish with a GitLab PAC token, you will need to have `GL_PAC_TOKEN` set and then run `sbt gitlabTokenPublish`

To publish with a GitLab CI token, you will need to have `CI_JOB_TOKEN` set and then run `sbt gitlabPublishCi`
