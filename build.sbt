sbtPlugin := true

name := "gitlab"
organization := "com.idbelabs.sbt"
version := sys.env.getOrElse("APP_VERSION", "0.1")

publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true)